#!/usr/bin/env python
import rospy
import tf
from visualization_msgs.msg import Marker
from sensor_msgs.msg import JointState
# robot module import
import four_link_solver as four_link


# function drawing the marker upon receiving new joint positions
def callback(data):

    # calculate dk of the robot
    pose = four_link.dk(data.position)

    marker = Marker()
    marker.header.frame_id = "base_link"
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = pose[3]
    marker.pose.position.y = pose[4]
    marker.pose.position.z = pose[5]
    quaternion = tf.transformations.quaternion_from_euler(pose[0], pose[1], pose[2])
    #type(pose) = geometry_msgs.msg.Pose
    marker.pose.orientation.x = quaternion[0]
    marker.pose.orientation.y = quaternion[1]
    marker.pose.orientation.z = quaternion[2]
    marker.pose.orientation.w = quaternion[3]

    marker.type = marker.CUBE
    marker.color.g = 1.0
    marker.color.a = 1.0
    marker.scale.x = 0.05
    marker.scale.y = 0.05
    marker.scale.z = 0.05

    publish_kinematics = rospy.Publisher('/four_link/direct_kinematics', Marker, queue_size=10)
    publish_kinematics.publish(marker)

# main funciton of the class
def four_link_direct_kinematics():

    rospy.init_node('four_link_direct_kinematics', anonymous=True)
    rospy.Subscriber("/joint_states", JointState, callback)

    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()

# class definition
if __name__ == '__main__':
    try:
        four_link_direct_kinematics()
    except rospy.ROSInterruptException:
        pass