#!/usr/bin/env python
import rospy
import tf
import numpy as np
from visualization_msgs.msg import Marker
from sensor_msgs.msg import JointState
# robot module import
import four_link_solver as four_link


# function drawing the marker upon receiving new joint positions
def callback(data):

    # calculate dk of the robot
    pose = four_link.dk(data.position)
    
    # calculate manipulability
    manipulability_v = four_link.manipulability_velocity(data.position)

    marker = Marker()
    marker.header.frame_id = "base_link"
    marker.pose.position.x = pose[3]
    marker.pose.position.y = pose[4]
    marker.pose.position.z = pose[5]
    quaternion = tf.transformations.quaternion_from_euler(manipulability_v[2], 0, 0)
    #type(pose) = geometry_msgs.msg.Pose
    marker.pose.orientation.x = quaternion[0]
    marker.pose.orientation.y = quaternion[1]
    marker.pose.orientation.z = quaternion[2]
    marker.pose.orientation.w = quaternion[3]

    marker.type = marker.SPHERE
    marker.color.g = 1.0
    marker.color.r = 0.7
    marker.color.a = 0.5
    marker.scale.x = 0.02
    marker.scale.y = 2*manipulability_v[0]
    marker.scale.z = 2*manipulability_v[1]

    publish_manip1 = rospy.Publisher('/four_link/manip_velocity', Marker, queue_size=10)
    publish_manip1.publish(marker)


    # calculate manipulability
    manipulability_f = four_link.manipulability_force(data.position)

    marker = Marker()
    marker.header.frame_id = "base_link"
    marker.pose.position.x = pose[3]
    marker.pose.position.y = pose[4]
    marker.pose.position.z = pose[5]
    quaternion = tf.transformations.quaternion_from_euler(manipulability_f[2], 0, 0)
    #type(pose) = geometry_msgs.msg.Pose
    marker.pose.orientation.x = quaternion[0]
    marker.pose.orientation.y = quaternion[1]
    marker.pose.orientation.z = quaternion[2]
    marker.pose.orientation.w = quaternion[3]

    marker.type = marker.SPHERE
    marker.color.g = 0.7
    marker.color.r = 1.0
    marker.color.a = 0.5
    marker.scale.x = 0.02
    marker.scale.y = 2*manipulability_f[0]
    marker.scale.z = 2*manipulability_f[1]

    publish_manip1 = rospy.Publisher('/four_link/manip_force', Marker, queue_size=10)

    publish_manip1.publish(marker)
    

# main funciton of the class
def four_link_manipulability_demo():

    rospy.init_node('four_link_manipulability_demo', anonymous=True)
    rospy.Subscriber("/joint_states", JointState, callback)

    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()

# class definition
if __name__ == '__main__':
    try:
        four_link_manipulability_demo()
    except rospy.ROSInterruptException:
        pass