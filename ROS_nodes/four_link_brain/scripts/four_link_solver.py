#!/usr/bin/env python
from math import sin, cos 
import numpy as np

# Physical parameters 
# URDF file can be parsed instead
# 
# base height
# link1
# link2
# link3
# link4
# gripper offset
# lengths of joint links
L = [0.05, 0.5, 0.5, 0.5, 0.5, 0.2]
# center of mass distance of each link
r = [0, 0.25, 0.25, 0.25, 0.25, 0.2]
# mass of each link
m = [0, 0.5, 0.5, 0.5, 0.5, 0]
# inertial of each link
I = []

# torque_limits
t_max = np.array([[1], [1], [1], [1]])

# direct kinematics functions for 4dof planar RRRR robot
def dk(q):
    x = 0
    y = -(L[1]*sin(q[0]) + L[2]*sin(q[0]+q[1]) + L[3]*sin(q[0]+q[1]+q[2]) + (L[4]+L[5])*sin(q[0]+q[1]+q[2]+q[3]))
    z = L[0] + L[1]*cos(q[0]) + L[2]*cos(q[0]+q[1]) + L[3]*cos(q[0]+q[1]+q[2]) + (L[4]+L[5])*cos(q[0]+q[1]+q[2]+q[3])
    roll = q[0]+q[1]+q[2]+q[3]
    return np.array([roll, 0, 0, x, y, z])

# direct kinematics functions for 4dof planar RRRR robot only roll, y and z
def dk_simple(q):
    y = -(L[1]*sin(q[0]) + L[2]*sin(q[0]+q[1]) + L[3]*sin(q[0]+q[1]+q[2]) + (L[4]+L[5])*sin(q[0]+q[1]+q[2]+q[3]))
    z = L[0] + L[1]*cos(q[0]) + L[2]*cos(q[0]+q[1]) + L[3]*cos(q[0]+q[1]+q[2]) + (L[4]+L[5])*cos(q[0]+q[1]+q[2]+q[3])
    roll = q[0]+q[1]+q[2]+q[3]
    return np.array([roll, y, z])

# jacobian function 6x4
def J(q):
    sq1 = sin(q[0])
    sq12 = sin(q[0] + q[1])
    sq123 = sin(q[0] + q[1] + q[2])
    sq1234 = sin(q[0] + q[1] + q[2] + q[3])
    cq1 = cos(q[0])
    cq12 = cos(q[0] + q[1])
    cq123 = cos(q[0] + q[1] + q[2])
    cq1234 = cos(q[0] + q[1] + q[2] + q[3])
    return np.array([
        [1, 1, 1, 1], 
        [0, 0, 0, 0], 
        [0, 0, 0, 0], 
        [0, 0, 0, 0],
        [-L[1]*cq1-L[2]*cq12-L[3]*cq123-(L[4]+L[5])*cq1234, -L[2]*cq12-L[3]*cq123-(L[4]+L[5])*cq1234, -L[3]*cq123-(L[4]+L[5])*cq1234, -(L[4]+L[5])*cq1234],
        [-L[1]*sq1-L[2]*sq12-L[3]*sq123-(L[4]+L[5])*sq1234, -L[2]*sq12-L[3]*sq123-(L[4]+L[5])*sq1234, -L[3]*sq123-(L[4]+L[5])*sq1234, -(L[4]+L[5])*sq1234]
    ])

# jacobian function only roll, y and z 3x4
def J_simple(q):
    sq1 = sin(q[0])
    sq12 = sin(q[0] + q[1])
    sq123 = sin(q[0] + q[1] + q[2])
    sq1234 = sin(q[0] + q[1] + q[2] + q[3])
    cq1 = cos(q[0])
    cq12 = cos(q[0] + q[1])
    cq123 = cos(q[0] + q[1] + q[2])
    cq1234 = cos(q[0] + q[1] + q[2] + q[3])
    return np.array([
        [1, 1, 1, 1], 
        [-L[1]*cq1-L[2]*cq12-L[3]*cq123-(L[4]+L[5])*cq1234, -L[2]*cq12-L[3]*cq123-(L[4]+L[5])*cq1234, -L[3]*cq123-(L[4]+L[5])*cq1234, -(L[4]+L[5])*cq1234], 
        [-L[1]*sq1-L[2]*sq12-L[3]*sq123-(L[4]+L[5])*sq1234, -L[2]*sq12-L[3]*sq123-(L[4]+L[5])*sq1234, -L[3]*sq123-(L[4]+L[5])*sq1234, -(L[4]+L[5])*sq1234]
    ])

def J_pseudo_inv(J):
    return np.linalg.inv(np.transpose(J).dot(J)).dot(np.transpose(J))

# iterative solving of inverse kinematics
def ik_solve(x_d, q_0, iterations):
    q = q_0
    damping = 0.1
    for i in range(iterations):
        e = (x_d - dk(q))
        # jacobian transpose method
        # q = q + 0.1*np.transpose(four_link_jacobian(q)).dot(e)
        # damped pseudo-inverse method
        q = q + np.linalg.inv(np.transpose(J(q)).dot(J(q)) + damping*np.identity(4)).dot(np.transpose(J(q))).dot(e)
    return q


def gravity_torque(q):
    q1 = q[0]
    q2 = q[1]
    q3 = q[2]
    q4 = q[3]
    g = 9.81
    return np.array([g*(-L[1]*m[2]*sin(q1) - L[1]*m[3]*sin(q1) - L[1]*(m[4]+m[5])*sin(q1) - L[2]*m[3]*sin(q1 + q2) - L[2]*(m[4]+m[5])*sin(q1 + q2) - L[3]*(m[4]+m[5])*sin(q1 + q2 + q3) - m[1]*r[1]*sin(q1) - m[2]*r[2]*sin(q1 + q2) - m[3]*r[3]*sin(q1 + q2 + q3) - (m[4]+m[5])*r[4]*sin(q1 + q2 + q3 + q4)),
         g*(-L[2]*m[3]*sin(q1 + q2) - L[2]*(m[4]+m[5])*sin(q1 + q2) - L[3]*(m[4]+m[5])*sin(q1 + q2 + q3) - m[2]*r[2]*sin(q1 + q2) - m[3]*r[3]*sin(q1 + q2 + q3) - (m[4]+m[5])*r[4]*sin(q1 + q2 + q3 + q4)),
         g*(-L[3]*(m[4]+m[5])*sin(q1 + q2 + q3) - m[3]*r[3]*sin(q1 + q2 + q3) - (m[4]+m[5])*r[4]*sin(q1 + q2 + q3 + q4)),
        -g*(m[4]+m[5])*r[4]*sin(q1 + q2 + q3 + q4)])

def mass_matrix(q):
    q1 = q[0]
    q2 = q[1]
    q3 = q[2]
    q4 = q[3]
    return np.matrix([[L[1]**2*m[2] + L[1]**2*m[3] + L[1]**2*(m[4] + m[5]) + 2*L[1]*L[2]*m[3]*cos(q2) + 2*L[1]*L[2]*(m[4] + m[5])*cos(q2) + 2*L[1]*L[3]*(m[4] + m[5])*cos(q2 + q3) + 2*L[1]*m[2]*r[2]*cos(q2) + 2*L[1]*m[3]*r[3]*cos(q2 + q3) + 2*L[1]*(m[4] + m[5])*r[4]*cos(q2 + q3 + q4) + L[2]**2*m[3] + L[2]**2*(m[4] + m[5]) + 2*L[2]*L[3]*(m[4] + m[5])*cos(q3) + 2*L[2]*m[3]*r[3]*cos(q3) + 2*L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]**2*(m[4] + m[5]) + 2*L[3]*(m[4] + m[5])*r[4]*cos(q4) + m[1]*r[1]**2 + m[2]*r[2]**2 +m[3]*r[3]**2 + (m[4] + m[5])*r[4]**2, L[1]*L[2]*m[3]*cos(q2) + L[1]*L[2]*(m[4] + m[5])*cos(q2) + L[1]*L[3]*(m[4] + m[5])*cos(q2 + q3) + L[1]*m[2]*r[2]*cos(q2) + L[1]*m[3]*r[3]*cos(q2 + q3) + L[1]*(m[4] + m[5])*r[4]*cos(q2 + q3 + q4) + L[2]**2*m[3] + L[2]**2*(m[4] + m[5]) + 2*L[2]*L[3]*(m[4] + m[5])*cos(q3) + 2*L[2]*m[3]*r[3]*cos(q3) + 2*L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]**2*(m[4] + m[5]) + 2*L[3]*(m[4] + m[5])*r[4]*cos(q4) + m[2]*r[2]**2 + m[3]*r[3]**2 + (m[4] + m[5])*r[4]**2, L[1]*L[3]*(m[4] + m[5])*cos(q2 + q3) + L[1]*m[3]*r[3]*cos(q2 + q3) + L[1]*(m[4] + m[5])*r[4]*cos(q2 + q3 + q4) + L[2]*L[3]*(m[4] + m[5])*cos(q3) + L[2]*m[3]*r[3]*cos(q3) + L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]**2*(m[4] + m[5]) + 2*L[3]*(m[4] + m[5])*r[4]*cos(q4) + m[3]*r[3]**2 + (m[4] + m[5])*r[4]**2, L[1]*(m[4] + m[5])*r[4]*cos(q2 + q3 + q4) + L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]*(m[4] + m[5])*r[4]*cos(q4) + (m[4] + m[5])*r[4]**2],
        [ L[1]*L[2]*m[3]*cos(q2) + L[1]*L[2]*(m[4] + m[5])*cos(q2) + L[1]*L[3]*(m[4] + m[5])*cos(q2 + q3) + L[1]*m[2]*r[2]*cos(q2) + L[1]*m[3]*r[3]*cos(q2 + q3) +L[1]*(m[4] + m[5])*r[4]*cos(q2 + q3 + q4) + L[2]**2*m[3] + L[2]**2*(m[4] + m[5]) + 2*L[2]*L[3]*(m[4] + m[5])*cos(q3) + 2*L[2]*m[3]*r[3]*cos(q3) + 2*L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]**2*(m[4] + m[5]) + 2*L[3]*(m[4] + m[5])*r[4]*cos(q4) + m[2]*r[2]**2 +m[3]*r[3]**2 + (m[4] + m[5])*r[4]**2, L[2]**2*m[3] + L[2]**2*(m[4] + m[5]) + 2*L[2]*L[3]*(m[4] + m[5])*cos(q3) + 2*L[2]*m[3]*r[3]*cos(q3) + 2*L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]**2*(m[4] + m[5]) + 2*L[3]*(m[4] + m[5])*r[4]*cos(q4) + m[2]*r[2]**2 + m[3]*r[3]**2 + (m[4] + m[5])*r[4]**2, L[2]*L[3]*(m[4] + m[5])*cos(q3) + L[2]*m[3]*r[3]*cos(q3) + L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]**2*(m[4] + m[5]) + 2*L[3]*(m[4] + m[5])*r[4]*cos(q4) + m[3]*r[3]**2 + (m[4] + m[5])*r[4]**2, L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]*(m[4] + m[5])*r[4]*cos(q4) + (m[4] + m[5])*r[4]**2],
        [ L[1]*L[3]*(m[4] + m[5])*cos(q2 + q3) + L[1]*m[3]*r[3]*cos(q2 + q3) + L[1]*(m[4] + m[5])*r[4]*cos(q2 + q3 + q4) + L[2]*L[3]*(m[4] + m[5])*cos(q3) + L[2]*m[3]*r[3]*cos(q3) + L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]**2*(m[4] + m[5]) + 2*L[3]*(m[4] + m[5])*r[4]*cos(q4) +m[3]*r[3]**2 + (m[4] + m[5])*r[4]**2, L[2]*L[3]*(m[4] + m[5])*cos(q3) + L[2]*m[3]*r[3]*cos(q3) + L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]**2*(m[4] + m[5]) + 2*L[3]*(m[4] + m[5])*r[4]*cos(q4) + m[3]*r[3]**2 + (m[4] + m[5])*r[4]**2, L[3]**2*(m[4] + m[5]) + 2*L[3]*(m[4] + m[5])*r[4]*cos(q4) + m[3]*r[3]**2 + (m[4] + m[5])*r[4]**2, L[3]*(m[4] + m[5])*r[4]*cos(q4) + (m[4] + m[5])*r[4]**2],
        [ L[1]*(m[4] + m[5])*r[4]*cos(q2 + q3 + q4) + L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]*(m[4] + m[5])*r[4]*cos(q4) + (m[4] + m[5])*r[4]**2, L[2]*(m[4] + m[5])*r[4]*cos(q3 + q4) + L[3]*(m[4] + m[5])*r[4]*cos(q4) + (m[4] + m[5])*r[4]**2, L[3]*(m[4] + m[5])*r[4]*cos(q4) + (m[4] + m[5])*r[4]**2, (m[4] + m[5])*r[4]**2]])

def equivalent_mass(q):
    # mass matrix calculation
    M = mass_matrix(q)
    # jacobian calculation
    Jac = J(q)
    # take only y and z direction
    Jac = Jac[4:, :]
    # inverse of equivalent mass 
    m_inv = Jac.dot(np.linalg.inv(M).dot(np.transpose(Jac)))
    # limit the infinite mass 
    m_inv[m_inv == 0] = 0.01
    # calculate the inverted mass
    return  1/m_inv

# velocity manipulability calculation
def manipulability_velocity(q):
    # jacobian calculation
    Jac = J(q)
    # take only y and z direction
    Jac = Jac[4:, :]
    # calculate the singular value decomposition
    U, S, V = np.linalg.svd(Jac)
    # return the singular values and the unit vector angle
    return [ S[0], S[1], np.arctan2(U[0,1],U[0,0]) ]

# force manipulability calculation
def manipulability_force(q):
    # jacobian calculation
    Jac = J(q)
    # take only y and z direction
    Jac = Jac[4:, :]
    # calculate the singular value decomposition
    U, S, V = np.linalg.svd(Jac)
    # return the singular values and the unit vector angle
    return [1/S[0], 1/S[1], np.arctan2(U[0,1],U[0,0])]

# maximal end effector force
def force_polytope(q):
    #q = [0, np.pi/3, -np.pi/10, np.pi/4]
    # jacobian calculation
    Jac = J(q)
    # take only y and z direction
    Jac = Jac[4:, :]
    Jac_invT = J_pseudo_inv(np.transpose(Jac))


    # calculate polytopes 
    # paper Evaluation of Force Capabilities for Redundant manipulators
    # P.Chiacchio et al.
    U, S, V = np.linalg.svd(Jac)
    r = np.linalg.matrix_rank(Jac)
    V1 = np.array(V.transpose()[:,:r])
    n = 4
    m = 2
    
    # create the 2n*(2n+m) matrix
    A = np.vstack((np.hstack((V1, np.identity(n), np.zeros((n,n)))), np.hstack((-V1, np.zeros((n,n)), np.identity(n)))))
    t_vertex = []
    # find all the combinations of submatrix with m zero slack variables
    for i in range(0,8):
        for j in range(i+1, 8):
            # exclude case when both i-th and n+i-th slack variable are zero
            if ( j == (n + i) ):
                continue

            # remove the i-th and j-th slack variable
            A_ = A
            A_ = np.delete(A_, [m+i, m+j], axis=1)
            # if not singular matrix - solve system
            if( np.linalg.matrix_rank(A_) == 2*n ):
                x = np.linalg.inv(A_).dot( np.vstack((t_max,t_max)) )
                slack_variables = x[2:]
                alpha = x[0:2]
                # if all slack variables positive vertex found
                if( min(slack_variables) >= 0 ):
                    t_v = (alpha[0]*V1[:,0]+alpha[1]*V1[:,1])# - gravity_torque(q)
                    if(t_vertex == []):
                        t_vertex = t_v
                    t_vertex = np.vstack((t_vertex, t_v))
    # calculate the forces based on the vertex torques
    return Jac_invT.dot( t_vertex.transpose())


# maximal end effector force
def force_polytope_auctus(q):
    #q = [0, np.pi/3, -np.pi/10, np.pi/4]
    # jacobian calculation
    Jac = J(q)
    # take only y and z direction
    Jac = Jac[4:, :]
    Jac_invT = J_pseudo_inv(np.transpose(Jac))

    # calculate polytope vertices 
    U, S, V = np.linalg.svd(Jac)
    r = np.linalg.matrix_rank(Jac)
    V1 = np.array(V.transpose()[:,:r])
    n = 4
    m = 2

    # all possible combinations for n=4 (4DOF)
    t_combination = np.array([[1, -1, 1, -1],[1, 1, -1, -1]])
    
    t_vertex = []
    # find all the combinations of submatrix with m zero slack variables
    for i in range(n):
        for j in range(i+1, n):

            # constructing the perturbed limits vector
            t_m = t_max[[i,j]]*np.identity(m).dot(t_combination)

            # take the i-th and j-th row
            V_ = V1[[i, j],:]

            # if not singular matrix - solve system
            if( np.linalg.matrix_rank( V_ ) == m ):
                # calculate alphas
                alpha = np.linalg.inv( V_ ).dot( t_m )
                # see if constraint satisfied and choose only the ones which do
                t_zero = abs((t_max - abs(V1.dot(alpha))).min(axis=0)) < 1e-5
                alpha = alpha[:,np.where(t_zero)[0]]
                # vertex torques calculation
                t_v = V1.dot( alpha )

                # save vertex torques
                if(t_vertex == []):
                    t_vertex = t_v
                t_vertex = np.hstack((t_vertex, t_v))

    # calculate the forces based on the vertex torques
    return Jac_invT.dot( t_vertex )

def force_polytope_ordered(q):
    #force_vertex = force_polytope(q)
    force_vertex = force_polytope_auctus(q)
    
    fx = force_vertex[0,:]
    fy = force_vertex[1,:]
    mean_x = np.mean(fx)
    mean_y = np.mean(fy)
    angles = np.arctan2( (fy-mean_y), (fx-mean_x))

    sort_index = np.argsort(angles)
    fx = np.take(fx, sort_index)
    fy = fy[sort_index]
    return np.vstack((fx,fy))
# definition of the four_link_solver module
if __name__ == '__main__':
    four_link_solver() 