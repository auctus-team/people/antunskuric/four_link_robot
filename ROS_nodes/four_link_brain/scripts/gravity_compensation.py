#!/usr/bin/env python
import rospy
import math
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState
import four_link_solver as four_link

def set_torque(data):

    q = data.position
    # compensate gravity
    torque = four_link.gravity_torque(q)

    pub_joint0 = rospy.Publisher('/four_link/joint0_torque_controller/command', Float64, queue_size=10)
    pub_joint1 = rospy.Publisher('/four_link/joint1_torque_controller/command', Float64, queue_size=10)
    pub_joint2 = rospy.Publisher('/four_link/joint2_torque_controller/command', Float64, queue_size=10)
    pub_joint3 = rospy.Publisher('/four_link/joint3_torque_controller/command', Float64, queue_size=10)

    pub_joint0.publish(torque[0])
    pub_joint1.publish(torque[1])
    pub_joint2.publish(torque[2])
    pub_joint3.publish(torque[3])

def test_torque():


    rospy.init_node('test_torque', anonymous=True)

    rospy.Subscriber("/four_link/joint_states", JointState, set_torque)
    
    rate = rospy.Rate(10) # 0hz

    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    try:
        test_torque()
    except rospy.ROSInterruptException:
        pass