#!/usr/bin/env python
import rospy
import tf
from visualization_msgs.msg import Marker
from sensor_msgs.msg import JointState
# robot module import
import four_link_solver as four_link


# function drawing the marker upon receiving new joint positions
def callback(data):

    # calculate dk of the robot
    pose = four_link.dk(data.position)
    eq_mass = four_link.equivalent_mass(data.position)
    

    marker = Marker()
    marker.header.frame_id = "base_link"
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = pose[3]
    marker.pose.position.y = pose[4]
    marker.pose.position.z = pose[5]
    quaternion = tf.transformations.quaternion_from_euler(0, 0, 1.57)
    #type(pose) = geometry_msgs.msg.Pose
    marker.pose.orientation.x = quaternion[0]
    marker.pose.orientation.y = quaternion[1]
    marker.pose.orientation.z = quaternion[2]
    marker.pose.orientation.w = quaternion[3]

    marker.type = marker.ARROW
    marker.color.g = 1.0
    marker.color.b = 1.0
    marker.color.a = 1.0
    marker.scale.x = eq_mass[0,0] 
    marker.scale.y = 0.02
    marker.scale.z = 0.02

    publish_eqmass = rospy.Publisher('/four_link/eqmass_y', Marker, queue_size=10)

    publish_eqmass.publish(marker)
    marker = Marker()
    marker.header.frame_id = "base_link"
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = pose[3]
    marker.pose.position.y = pose[4]
    marker.pose.position.z = pose[5]
    quaternion = tf.transformations.quaternion_from_euler(0, 1.57, 0)
    #type(pose) = geometry_msgs.msg.Pose
    marker.pose.orientation.x = quaternion[0]
    marker.pose.orientation.y = quaternion[1]
    marker.pose.orientation.z = quaternion[2]
    marker.pose.orientation.w = quaternion[3]

    marker.type = marker.ARROW
    marker.color.g = 0.7
    marker.color.b = 1.0
    marker.color.a = 1.0
    marker.scale.x = eq_mass[1,1]
    marker.scale.y = 0.02
    marker.scale.z = 0.02

    publish_eqmass = rospy.Publisher('/four_link/eqmass_z', Marker, queue_size=10)
    publish_eqmass.publish(marker)

# main funciton of the class
def four_link_equivalent_mass():

    rospy.init_node('four_link_equivalent_mass', anonymous=True)
    rospy.Subscriber("/joint_states", JointState, callback)

    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()

# class definition
if __name__ == '__main__':
    try:
        four_link_equivalent_mass()
    except rospy.ROSInterruptException:
        pass