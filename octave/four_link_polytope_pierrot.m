# calcualte polytopes 
# paper Evaluation of Force Capabilities for Redundant manipulatiors
# P.Chiacchio et al.
[u s v] = svd(J_n);
r = rank(J_n);
V1 = v(:,1:r);
n = 4;
m = 2;

A = [V1, eye(n) zeros(n); -V1, zeros(n) eye(n)];
f_vertex_xy = [];
t_vertex = [];
alpha_vertex = [];

# find all the combinations of submatrix with m zero slack variables
for i = 1:7
  for j = i+1:8
    # exclude case when both i-th and n+i-th slack variable are zero
    if ( j == (n + i) ) 
      continue;
    end
    
    # remove the i-th and j-th slack variable
    A_ = A;
    A_(:,[m+i, m+j]) = [];
    
    # if not singular matrix - solve system
    if( rank(A_) == 2*n)
      x = inv(A_)*[t_max, t_max]';
      slack_variables = x(3:end);
      alpha = x(1:2)';
      # if all slack variables positive vertex found
      if( ~any(slack_variables < 0) )
        disp('vertex found')
        alpha_vertex = [alpha_vertex alpha'];
        t_vertex = [t_vertex sum(alpha.*V1, 2)];
      end
    end
  end
end

# calculate the forces based on the vertex torques
f_vertex_xy = J_n_invT*( t_vertex );
        
# plot vertexes
plot( f_vertex_xy(1,:), f_vertex_xy(2,:), 'ks', 'LineWidth', 2);
