# calcualte polytope vertices 
# version 04.09.2020
# 
[u s v] = svd(J_n);
r = rank(J_n);
V1 = v(:,1:r);
n = 4;
m = 2;

t_max_pert = [1 -1 1 -1; 1 1 -1 -1];

f_vertex_xy = [];
t_vertex = [];
alpha_vertex = [];

# find all the combinations of submatrix with m zero slack variables
for i = 1:3
  for j = i+1:4
    
    t_m = diag(t_max([i,j]))*t_max_pert;
    
    # take the i-th and j-th row
    V_ = V1([i, j],:);
    # if not singular matrix - solve system
    if( rank(V_) == m)
      alpha = inv(V_)*t_m;
      
      t_zero = abs(min( t_max - abs(V1*alpha) ));
      alpha(:,find(t_zero > 1e-5)) = [];
      
      # if all slack variables positive vertex found
      alpha_vertex = [alpha_vertex alpha];
      t_vertex = [t_vertex V1*alpha];
    end
  end
end

# calculate the forces based on the vertex torques
f_vertex_xy = J_n_invT*( t_vertex );
        
# plot vertexes
plot( f_vertex_xy(1,:), f_vertex_xy(2,:), 'ks', 'LineWidth', 2);